import controller.DrawingLoop;
import controller.GameLoop;
import de.saxsys.mvvmfx.testingutils.jfxrunner.JfxRunner;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import model.Ball;
import model.Character;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import view.Platform;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

@RunWith(JfxRunner.class)
public class CharacterTest {
    private Character floatingCharacter, floatingCharacter2;
    private ArrayList<Character> characterListUnderTest;
    private Ball ballUnderTest;
    private Platform platformUnderTest;
    private GameLoop gameLoopUnderTest;
    private DrawingLoop drawingLoopUnderTest;
    private Method updateMethod;
    private Method redrawMethod;
    private Method checkReachFloorMethod;
    private Method checkReachWallMethod;
    private Method checkReachHighestMethod;
    private Method redrawBallMethod;

    @Before
    public void setup() {
        floatingCharacter = new Character(30, 200, 0, 0, KeyCode.A, KeyCode.D, KeyCode.W, KeyCode.B,KeyCode.V);
        floatingCharacter2 = new Character(Platform.WIDTH-60, 200,0,0, KeyCode.LEFT,KeyCode.RIGHT,KeyCode.UP, KeyCode.L,KeyCode.K);
        ballUnderTest = new Ball(30,200);
        characterListUnderTest = new ArrayList<Character>();
        characterListUnderTest.add(floatingCharacter);
        characterListUnderTest.add(floatingCharacter2);
        platformUnderTest = new Platform();
        gameLoopUnderTest = new GameLoop(platformUnderTest);
        drawingLoopUnderTest = new DrawingLoop(platformUnderTest);

        try {
            updateMethod = GameLoop.class.getDeclaredMethod("update", ArrayList.class);
            redrawMethod = DrawingLoop.class.getDeclaredMethod("paint", ArrayList.class);
            redrawBallMethod = DrawingLoop.class.getDeclaredMethod("paint", Ball.class);
            checkReachFloorMethod = Character.class.getDeclaredMethod("checkReachFloor");
            checkReachWallMethod = Character.class.getDeclaredMethod("checkReachGameWall");
            checkReachHighestMethod = Character.class.getDeclaredMethod("checkReachHighest");
            checkReachHighestMethod.setAccessible(true);
            checkReachWallMethod.setAccessible(true);
            updateMethod.setAccessible(true);
            redrawMethod.setAccessible(true);
            redrawBallMethod.setAccessible(true);
            checkReachFloorMethod.setAccessible(true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            updateMethod = null;
            redrawMethod = null;
            checkReachFloorMethod = null;
            checkReachWallMethod = null;
        }
    }

    @Test
    public void characterInitialValuesShouldMatchConstructorArguments() {
        assertEquals("Initialx", 30, floatingCharacter.getX(), 0);
        assertEquals("Initialy", 200, floatingCharacter.getY(), 0);
        assertEquals("Offsetx", 0, floatingCharacter.getOffsetX(), 0.0);
        assertEquals("Offsety", 0, floatingCharacter.getOffsetY(), 0.0);
        assertEquals("Leftkey", KeyCode.A, floatingCharacter.getLeftKey());
        assertEquals("Rightkey", KeyCode.D, floatingCharacter.getRightKey());
        assertEquals("Upkey", KeyCode.W, floatingCharacter.getUpKey());
        assertEquals("Powerkey", KeyCode.B, floatingCharacter.getPowerKey());
    }

    @Test
    public void characterShouldMoveToTheLeftAfterTheLeftKeyIsPressed() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException {
        Character characterUnderTest = characterListUnderTest.get(0);
        int startX = characterUnderTest.getX();
        platformUnderTest.getKeys().add(KeyCode.A);
        updateMethod.invoke(gameLoopUnderTest, characterListUnderTest);
        redrawMethod.invoke(drawingLoopUnderTest, characterListUnderTest);
        Field isMoveLeft = characterUnderTest.getClass().getDeclaredField("isMoveLeft");
        isMoveLeft.setAccessible(true);
        assertTrue("Controller:Leftkeypressingisacknowledged", platformUnderTest.getKeys().isPressed(KeyCode.A));
        assertTrue("Model:Charactermovingleftstateisset", isMoveLeft.getBoolean(characterUnderTest));
        assertTrue("View:Characterismovingleft", characterUnderTest.getX() < startX);
    }

    @Test
    public void characterShouldMoveToTheRightAfterTheRightKeyIsPressed() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException {
        Character characterUnderTest = characterListUnderTest.get(0);
        int startX = characterUnderTest.getX();
        platformUnderTest.getKeys().add(KeyCode.D);
        updateMethod.invoke(gameLoopUnderTest, characterListUnderTest);
        redrawMethod.invoke(drawingLoopUnderTest, characterListUnderTest);
        Field isMoveRight = characterUnderTest.getClass().getDeclaredField("isMoveRight");
        isMoveRight.setAccessible(true);
        assertTrue("Controller:Rightkeypressingisacknowledged", platformUnderTest.getKeys().isPressed(KeyCode.D));
        assertTrue("Model:Charactermovingrightstateisset", isMoveRight.getBoolean(characterUnderTest));
        assertTrue("View:Characterismovingright", characterUnderTest.getX() > startX);
    }
    @Test
    public void characterShouldMoveWithHighSpeedAfterTheDashKeyIsPressed() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException {
        Character characterUnderTest = characterListUnderTest.get(0);
        int startX = characterUnderTest.getX();
        platformUnderTest.getKeys().add(KeyCode.D);
        platformUnderTest.getKeys().add(KeyCode.V);
        updateMethod.invoke(gameLoopUnderTest, characterListUnderTest);
        redrawMethod.invoke(drawingLoopUnderTest, characterListUnderTest);
        Field isMoveRight = characterUnderTest.getClass().getDeclaredField("isMoveRight");
        Field velocity = characterUnderTest.getClass().getDeclaredField("xVelocity");
        velocity.setAccessible(true);
        isMoveRight.setAccessible(true);
        assertTrue("Controller:Rightkeypressingisacknowledged", platformUnderTest.getKeys().isPressed(KeyCode.D));
        assertTrue("Model:Charactermovingrightstateisset", isMoveRight.getBoolean(characterUnderTest));
        assertTrue("View:Characterismovingright", characterUnderTest.getX() > startX);
        assertEquals("SPEED = 10", 10, velocity.getInt(characterUnderTest));
    }

    @Test
    public void characterShouldBeAbleToJumpAfterTheJumpKeyIsPressedWhileOnGround() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException, InterruptedException {
        System.out.println("==================Test Jumping from ground==================");
        long start = System.nanoTime();
        long end = start + TimeUnit.SECONDS.toNanos(2); //2secs
        Character characterUnderTest = characterListUnderTest.get(0);
        boolean testpass = false;
        while (System.nanoTime() < end && !testpass) {
            updateMethod.invoke(gameLoopUnderTest, characterListUnderTest);
            redrawMethod.invoke(drawingLoopUnderTest, characterListUnderTest);
            Field jumping = characterUnderTest.getClass().getDeclaredField("jumping");
            Field canJump = characterUnderTest.getClass().getDeclaredField("canJump");
            Field falling = characterUnderTest.getClass().getDeclaredField("falling");
            jumping.setAccessible(true);
            canJump.setAccessible(true);
            falling.setAccessible(true);
            checkReachFloorMethod.invoke(characterUnderTest);
            if (canJump.getBoolean(characterUnderTest)) {
                int startY = characterUnderTest.getY();
                platformUnderTest.getKeys().add(KeyCode.W);
                updateMethod.invoke(gameLoopUnderTest, characterListUnderTest);
                redrawMethod.invoke(drawingLoopUnderTest, characterListUnderTest);
                assertTrue("Controller:Jumpkeypressingisacknowledged", platformUnderTest.getKeys().isPressed(KeyCode.W));
                assertTrue("Model:Characterjumpingstateisset", jumping.getBoolean(characterUnderTest));
                assertFalse("Model:Characterisnotfalling", falling.getBoolean(characterUnderTest));
                assertFalse("Model:Charactercannotjumpanymore", canJump.getBoolean(characterUnderTest));
                assertTrue("View:Characterisreallyjumping", characterUnderTest.getY() < startY);
                testpass = true;
            }
        }
    }

    @Test
    public void characterShouldNotBeAbleToJumpWhenOnAir() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException, InterruptedException {
        Character characterUnderTest = characterListUnderTest.get(0);
        updateMethod.invoke(gameLoopUnderTest, characterListUnderTest);
        redrawMethod.invoke(drawingLoopUnderTest, characterListUnderTest);
        int beforePressingAgainY = characterUnderTest.getY();
        platformUnderTest.getKeys().add(KeyCode.W);
        assertTrue("Controller:Jumpkeypressingisacknowledged", platformUnderTest.getKeys().isPressed(KeyCode.W));
        assertFalse("View:Characterisreallyjumping", characterUnderTest.getY() < beforePressingAgainY);
    }

    @Test
    public void ballShouldBeAbleToBounceAfterHittingCharacter() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException, InterruptedException, NoSuchMethodException {
        Character characterUnderTest = characterListUnderTest.get(0);
        Ball ballforTest = ballUnderTest;
        Method checkCollisionMethod = DrawingLoop.class.getDeclaredMethod("checkDrawCollisions", ArrayList.class, Ball.class);
        checkCollisionMethod.setAccessible(true);
        updateMethod.invoke(gameLoopUnderTest, characterListUnderTest);
        redrawMethod.invoke(drawingLoopUnderTest, characterListUnderTest);
        redrawBallMethod.invoke(drawingLoopUnderTest, ballforTest);
        checkCollisionMethod.invoke(drawingLoopUnderTest,characterListUnderTest,ballforTest);
        Field isBouncing = ballforTest.getClass().getDeclaredField("bouncing");
        isBouncing.setAccessible(true);
        assertTrue("The Ball is bounced", isBouncing.getBoolean(ballforTest));
    }
    @Test
    public void ballShouldBeAbleToGetSlammedAfterCharacterPowerHit() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException, InterruptedException, NoSuchMethodException {
        Character characterUnderTest = characterListUnderTest.get(0);
        Ball ballforTest = ballUnderTest;
        Method checkCollisionMethod = DrawingLoop.class.getDeclaredMethod("checkDrawCollisions", ArrayList.class, Ball.class);
        checkCollisionMethod.setAccessible(true);
        platformUnderTest.getKeys().add(KeyCode.B);
        updateMethod.invoke(gameLoopUnderTest, characterListUnderTest);
        redrawMethod.invoke(drawingLoopUnderTest, characterListUnderTest);
        redrawBallMethod.invoke(drawingLoopUnderTest, ballforTest);
        checkCollisionMethod.invoke(drawingLoopUnderTest,characterListUnderTest,ballforTest);
        Field isFalling = ballforTest.getClass().getDeclaredField("falling");
        isFalling.setAccessible(true);
        assertTrue("The Ball is knocked to the floor", isFalling.getBoolean(ballforTest));
    }

    @Test
    public void scoreOfTheOppositeShouldIncreaseAfterBallReachFloor() throws IllegalAccessException, InvocationTargetException, InvocationTargetException, InvocationTargetException, NoSuchFieldException, InterruptedException, NoSuchMethodException {
        Character characterUnderTest = characterListUnderTest.get(0);
        Ball ballforTest = new Ball(500,300);
        Method BallReachFloor = Ball.class.getDeclaredMethod("checkReachFloor");
        Method checkCollisionMethod = DrawingLoop.class.getDeclaredMethod("checkDrawCollisions", ArrayList.class, Ball.class);
        BallReachFloor.setAccessible(true);
        checkCollisionMethod.setAccessible(true);
        updateMethod.invoke(gameLoopUnderTest, characterListUnderTest);
        redrawMethod.invoke(drawingLoopUnderTest, characterListUnderTest);
        redrawBallMethod.invoke(drawingLoopUnderTest, ballforTest);
        checkCollisionMethod.invoke(drawingLoopUnderTest,characterListUnderTest,ballforTest);
        BallReachFloor.invoke(ballforTest);
        Field reachFloor = ballforTest.getClass().getDeclaredField("reachFloor");
        reachFloor.setAccessible(true);
        assertTrue("Ball reached floor", reachFloor.getBoolean(ballforTest));
        assertEquals("Score = 1", 1, characterUnderTest.getScore());
    }
}