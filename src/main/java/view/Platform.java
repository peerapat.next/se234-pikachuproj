package view;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import model.Ball;
import model.Character;
import model.Keys;
import model.Net;

import java.util.ArrayList;

public class Platform extends Pane {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 400;
    public static final int GROUND = 300;

    public Image platformImg;
    public ArrayList<Character> characterList;
    public Net net;
    public Ball ball;
    public ArrayList<Score>scoreList;
    public static Keys keys;
    public ImageView backgroundImg;

    public Platform() {
        this.characterList=new ArrayList<>();
        this.scoreList=new ArrayList();
        this.keys = new Keys();
        this.scoreList.add(new Score(30,GROUND+ 30));
        this.scoreList.add(new Score(Platform.WIDTH-60,GROUND+ 30));
        this.platformImg = new Image(getClass().getResourceAsStream("/assets/Background.png"));
        this.backgroundImg = new ImageView(platformImg);
        this.backgroundImg.setFitHeight(HEIGHT);
        this.backgroundImg.setFitWidth(WIDTH);
        this.characterList.add(new Character(30, 200,0,0, KeyCode.A,KeyCode.D,KeyCode.W,KeyCode.B,KeyCode.V));
        this.characterList.add(new Character(Platform.WIDTH-60, 200,0,0, KeyCode.LEFT,KeyCode.RIGHT,KeyCode.UP,KeyCode.L,KeyCode.K));
        this.net = new Net(395, Platform.GROUND-(Net.NET_HEIGHT-5));
        this.ball = new Ball(30, 0);
        getChildren().add(backgroundImg);
        getChildren().addAll(characterList);
        getChildren().add(net);
        getChildren().add(ball);
        getChildren().addAll(scoreList);
    }

    public ArrayList<Character> getCharacterList() {
        return characterList;
    }
    public Ball getBall() {
        return ball;
    }

    public static Keys getKeys() {
        return keys;
    }

    public ArrayList<Score> getScoreList() {
        return scoreList;
    }
}

