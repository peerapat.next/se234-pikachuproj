package view;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Score extends Pane {
    Label point;
    Label gauge;
    public Score(int x,int y) {
        point=new Label("0");
        gauge=new Label("0");
        setTranslateX(x);
        setTranslateY(y);
        point.setFont(Font.font("Verdana", FontWeight.BOLD,30));
        point.setTextFill(Color.web("#FFF"));
        gauge.setFont(Font.font("Verdana", FontWeight.BOLD,10));
        gauge.setTextFill(Color.web("#FE0000"));
        getChildren().addAll(point,gauge);
    }
    public void setPoint(int score) {
        this.point.setText(Integer.toString(score));
    }
    public void setGauge(int gauge) { this.gauge.setText(Integer.toString(gauge));}
}
