package controller;

import javafx.scene.input.KeyCode;
import model.Ball;
import model.Character;
import model.Net;
import view.Platform;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class DrawingLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    public DrawingLoop(Platform platform) {
        this.platform = platform;
        frameRate = 60;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void checkDrawCollisions(ArrayList<Character> characterList, Ball ball) throws InterruptedException {
        for (Character character : characterList ) {
            character.checkReachGameWall();
            character.checkReachHighest();
            character.checkReachFloor();
            character.checkReachNet();
        }
        ball.checkReachGameWall();
        ball.checkReachFloor();
        ball.checkReachHighest();
        ball.checkReachNet();
        for(Character cA: characterList) {
            for(Character cB:characterList) {
                if(cA!=cB) {
                    if(cA.getBoundsInParent().intersects(ball.getBoundsInParent()) && !platform.getKeys().isPressed(cA.getPowerKey())) {
                        ball.normalHit(2);
                        ball.hitCharacterTrace(cA.getThisCharacter());
                    }else if(cA.getBoundsInParent().intersects(ball.getBoundsInParent()) && platform.getKeys().isPressed(cA.getPowerKey())){
                        ball.powerHit(2);
                    }else if(cB.getBoundsInParent().intersects(ball.getBoundsInParent())&& !platform.getKeys().isPressed(cB.getPowerKey())){
                        ball.normalHit(1);
                        ball.hitCharacterTrace(cB.getThisCharacter());
                    }else if(cB.getBoundsInParent().intersects(ball.getBoundsInParent()) && platform.getKeys().isPressed(cB.getPowerKey())){
                        ball.powerHit(1);
                    }
                    else if(ball.reachedFloor()){
                        if(ball.getX()>400){
                            if(cA.getGauge() >= 5){
                            cA.setScore(3);
                            }else cA.setScore(1);
                            cA.resetGauge();
                            cB.increaseGauge();
                            platform.getKeys().remove(KeyCode.L);
                            platform.getKeys().remove(KeyCode.B);
                            TimeUnit.MILLISECONDS.sleep(500);
                            cA.respawn();
                            cB.respawn();
                            ball.setFirstserve(1);
                            ball.respawn();
                        }
                        else if(ball.getX()<400){
                            if(cB.getGauge() >= 5){
                            cB.setScore(3);
                            }else cB.setScore(1);
                            cB.resetGauge();
                            cA.increaseGauge();
                            platform.getKeys().remove(KeyCode.L);
                            platform.getKeys().remove(KeyCode.B);
                            TimeUnit.MILLISECONDS.sleep(500);
                            cA.respawn();
                            cB.respawn();
                            ball.setFirstserve(2);
                            ball.respawn();
                        }
                    }
                }
            }
        }
    }

    private void paint(ArrayList<Character> characterList) {
        for (Character character : characterList ) {
            character.repaint();
        }
    }
    private void paint(Ball ball){
        ball.repaint();
    }

    @Override
    public void run() {
        while (running) {

            float time = System.currentTimeMillis();

            try {
                checkDrawCollisions(platform.getCharacterList(),platform.getBall());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            paint(platform.getCharacterList());
            paint(platform.getBall());

            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }
}
