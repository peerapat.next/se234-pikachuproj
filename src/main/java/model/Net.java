package model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class Net extends Pane {
    public static final int NET_WIDTH = 10;
    public static final int NET_HEIGHT = 100;

    private Image img;
    private ImageView netImg;
    private int x;
    private int y;
    public Net(int x, int y){
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.img = new Image(getClass().getResourceAsStream("/assets/net.png"));
        this.netImg = new ImageView(img);
        this.netImg.setFitWidth(NET_WIDTH);
        this.netImg.setFitHeight(NET_HEIGHT);
        this.getChildren().addAll(this.netImg);
    }
}
