package model;

import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import org.apache.logging.log4j.Level;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import view.Platform;

import java.util.concurrent.TimeUnit;

public class Character extends Pane {

    Logger logger = LogManager.getLogger(Character.class.getCanonicalName());

    public int CHARACTER_WIDTH = 65;
    public int CHARACTER_HEIGHT = 65;

    private Image characterImg;
    private AnimatedSprite imageView;

    private int x;
    private int y;
    private int startX;
    private int startY;
    private int offsetX;
    private int offsetY;
    private int score=0;
    private KeyCode leftKey;
    private KeyCode rightKey;
    private KeyCode upKey;
    private KeyCode powerKey;
    private KeyCode dashKey;

    int xVelocity = 0;
    int yVelocity = 0;
    int xAcceleration = 1;
    int yAcceleration = 1;
    int xMaxVelocity = 5;
    int yMaxVelocity = 17;
    int powerGauge = 0;
    boolean isMoveLeft = false;
    boolean isMoveRight = false;
    boolean falling = true;
    boolean canJump = false;
    boolean jumping = false;
    boolean notRespawning = false;

    public Character(int x, int y, int offsetX, int offsetY, KeyCode leftKey, KeyCode rightKey, KeyCode upKey, KeyCode powerKey,KeyCode dashKey) {
        this.startX=x;
        this.startY=y;
        this.offsetX=offsetX;
        this.offsetY=offsetY;
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.characterImg = new Image(getClass().getResourceAsStream("/assets/pikasprite_sheet.png"));
        this.imageView = new AnimatedSprite(characterImg,5,5,offsetX,offsetY,65,65);
        this.imageView.setFitWidth(CHARACTER_WIDTH);
        this.imageView.setFitHeight(CHARACTER_HEIGHT);
        this.leftKey = leftKey;
        this.rightKey = rightKey;
        this.upKey = upKey;
        this.powerKey = powerKey;
        this.dashKey = dashKey;
        this.getChildren().addAll(this.imageView);
    }

    public void moveLeft() {
        isMoveLeft = true;
        isMoveRight = false;
    }
    public void moveRight() {
        isMoveRight = true;
        isMoveLeft = false;
    }

    public void stop() {
        isMoveLeft = false;
        isMoveRight = false;
        xVelocity = 0;
    }

    public void jump() {
        if (canJump) {
            yVelocity = yMaxVelocity;
            canJump = false;
            jumping = true;
            falling = false;
        }
    }

    public void checkReachHighest() {
        if(jumping &&  yVelocity <= 0) {
            jumping = false;
            falling = true;
            yVelocity = 0;
        }
        else if(jumping && yVelocity > 0 && !Platform.getKeys().isPressed(powerKey)) {
            this.getImageView().tickJump();
        }
        else if(jumping && yVelocity > 0 && Platform.getKeys().isPressed(powerKey)) {
            this.getImageView().tickPower();
        }
    }

    public void checkReachFloor() {
        if(falling && y >= Platform.GROUND - CHARACTER_HEIGHT) {
            falling = false;
            canJump = true;
            notRespawning = true;
            yVelocity = 0;
        }
    }

    public void checkReachGameWall() {
        if(x <= 0) {
            x = 0;
        } else if( x+getWidth() >= Platform.WIDTH) {
            x = Platform.WIDTH-CHARACTER_WIDTH;
        }
    }

    public void checkReachNet() {
        if(x+getWidth()>= 400-Net.NET_WIDTH && isMoveRight && startX<400){
            x=(400-Net.NET_WIDTH)-CHARACTER_WIDTH;
            stop();
        }else if(x+getWidth()<= Net.NET_WIDTH+465 && isMoveLeft&& startX>400) {
            x=(Net.NET_WIDTH+465)-CHARACTER_WIDTH;
            stop();
        }
    }

    public void moveX() {
        setTranslateX(x);

        if(isMoveLeft && !Platform.getKeys().isPressed(dashKey)) {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
            x = x - xVelocity;
        }else if(isMoveLeft && Platform.getKeys().isPressed(dashKey)) {
            xVelocity = 10;
            x = x - xVelocity;
        }
        if(isMoveRight && !Platform.getKeys().isPressed(dashKey)) {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
            x = x + xVelocity;
        }else if(isMoveRight && Platform.getKeys().isPressed(dashKey)) {
            xVelocity = 10;
            x = x + xVelocity;
        }
    }

    public void moveY() {
        setTranslateY(y);

        if(falling) {
            yVelocity = yVelocity >= yMaxVelocity? yMaxVelocity : yVelocity+yAcceleration;
            y = y + yVelocity;
        }
        else if(jumping) {
            yVelocity = yVelocity <= 0 ? 0 : yVelocity-yAcceleration;
            y = y - yVelocity;
        }
    }

    public void repaint() {
        moveX();
        moveY();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void respawn() {
        if(powerGauge<5) {
            this.CHARACTER_HEIGHT = 65;
            this.CHARACTER_WIDTH = 65;
            x=startX;
            y=startY;
            imageView.setFitWidth(CHARACTER_WIDTH);
            imageView.setFitHeight(CHARACTER_HEIGHT);
        }else{
            if(startX<400){
            x=startX;
            }else x=startX-65;
            this.CHARACTER_HEIGHT = 95;
            this.CHARACTER_WIDTH = 95;
            y=startY-100;
            imageView.setFitWidth(CHARACTER_WIDTH);
            imageView.setFitHeight(CHARACTER_HEIGHT);
        }
        isMoveLeft=false;
        isMoveRight=false;
        falling=true;
        canJump=false;
        jumping=false;
        notRespawning=false;
    }

    public void movedToX() {
        logger.info("x:{} vx:{}",x,xVelocity);
    }
    public void jumpedToY() {
        logger.info("isJumping:{} y:{}",jumping,y);
    }
    public void traceScore() {
        logger.info("Score:{}",score);
    }

    public KeyCode getLeftKey() {
        return leftKey;
    }

    public KeyCode getRightKey() {
        return rightKey;
    }

    public KeyCode getUpKey() {
        return upKey;
    }

    public AnimatedSprite getImageView() { return imageView; }

    public boolean getFalling() {return falling;}

    public int getScore() {
        return score;
    }
    public void setScore(int add) {
        score = score+add;
        traceScore();
    }

    public double getOffsetX() {
        return offsetX;
    }

    public double getOffsetY() {
        return offsetY;
    }
    public String getThisCharacter(){
        return this.toString();
    }

    public KeyCode getPowerKey() {
        return powerKey;
    }

    public void increaseGauge(){
        powerGauge++;
    }
    public void resetGauge(){
        powerGauge = 0;
    }

    public int getGauge() {
        return powerGauge;
    }
}
