package model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import view.Platform;

public class Ball extends Pane {
    Logger logger = LogManager.getLogger(Character.class.getCanonicalName());

    public static final int BALL_RADIUS = 40;

    boolean isMoveLeft = false;
    boolean isMoveRight = false;
    boolean falling = true;
    boolean bouncing = false;
    boolean reachFloor = false;

    private Image img;
    private ImageView ballImg;

    private int firstserve = 1;
    private int x;
    private double y;
    private int startX;
    private int startY;
    int xVelocity = 0;
    double yVelocity = 0;
    int xAcceleration = 1;
    double yAcceleration = 0.3;
    int xMaxVelocity = 7;
    double yMaxVelocity = 10;
    public Ball(int x, int y){
        this.x = x;
        this.y = y;
        this.startX = x;
        this.startY = y;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.xVelocity = 0;
        this.yVelocity = 1;
        this.img = new Image(getClass().getResourceAsStream("/assets/ball.png"));
        this.ballImg = new ImageView(img);
        this.ballImg.setFitWidth(BALL_RADIUS);
        this.ballImg.setFitHeight(BALL_RADIUS);
        this.getChildren().addAll(this.ballImg);
    }
    public void moveLeft() {
        isMoveLeft = true;
        isMoveRight = false;
    }
    public void moveRight() {
        isMoveRight = true;
        isMoveLeft = false;
    }
    public void checkReachFloor() {
        if(falling && y >= Platform.GROUND-20) {
            falling = false;
            yVelocity = 0;
            reachFloor = true;
            hitFloorTrace();
        }
    }
    public void checkReachNet() {
        if(x+getWidth()>= 400-Net.NET_WIDTH && y+getHeight()>= 400 && isMoveRight){
            wallHit(2);
        }else if(x+getWidth()<= Net.NET_WIDTH+465 && y+getHeight()>= 400 && isMoveLeft) {
            wallHit(1);
        }
    }
    public void checkReachHighest() {
        if(yVelocity <= 0) {
            falling = true;
            bouncing = false;
            yVelocity = 0;
        }
    }
    public void checkReachGameWall() {
        if(x <= 0) {
            x = 0;
            wallHit(1);
        } else if( x+getWidth() >= Platform.WIDTH) {
            x = Platform.WIDTH-BALL_RADIUS;
            wallHit(2);
        }
    }
    public void normalHit(int characterwhoHits) {
        bouncing = true;
        yVelocity = yMaxVelocity;
        falling = false;
        if(characterwhoHits == 1){
            moveRight();
            moveY();
        }
        else if(characterwhoHits ==2){
            moveLeft();
            moveY();
        }
    }
    public void powerHit(int characterwhoHits) {
        bouncing = false;
        yVelocity = yMaxVelocity/2;
        xVelocity = 11;
        falling = true;
        if(characterwhoHits == 1){
            moveRight();
            moveY();
        }
        else if(characterwhoHits ==2){
            moveLeft();
            moveY();
        }
    }
    public void wallHit(int wallthatHits) {
        hitBorderTrace();
        bouncing = false;
        falling = true;
        if(wallthatHits == 1){
            moveRight();
            moveY();
        }
        else if(wallthatHits ==2){
            moveLeft();
            moveY();
        }
    }
    public void moveY() {
        setTranslateY(y);

        if(falling) {
            yVelocity = yVelocity >= yMaxVelocity? yMaxVelocity : yVelocity+yAcceleration;
            y = y + yVelocity;
        } else if(bouncing){
            yVelocity = yVelocity <= 0 ? 0 : yVelocity-yAcceleration;
            y = y - yVelocity;
        }
    }
    public void moveX() {
        setTranslateX(x);

        if(isMoveLeft) {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
            x = x - xVelocity;
        }
        if(isMoveRight) {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
            x = x + xVelocity;
        }
    }
    public void respawn() {
        if(firstserve == 1){
            x=startX;
            y=startY;
        }else if(firstserve ==2){
            x=740;
            y=startY;
        }
        isMoveLeft=false;
        isMoveRight=false;
        falling=true;
        bouncing=false;
        reachFloor=false;
    }
    public void repaint() {
        moveX();
        moveY();
    }

    public boolean reachedFloor(){
        return reachFloor;
    }

    public int getX(){
        return x;
    }
    public void setFirstserve(int server){
        firstserve = server;
    }
    public void hitBorderTrace(){
        logger.info("Hit the border");
    }
    public void hitCharacterTrace(String name){
        logger.info("Hit by Character: {}",name);
    }
    public void hitFloorTrace(){
        logger.info("Hit the floor");
    }
}
